"use strict";

let stepGrid = function() {

  function materialsForColors(colorsByName) {
    let out = {};
    Object.keys(colorsByName).forEach(function(name) {
      let material = new THREE.MeshBasicMaterial({color: colorsByName[name]});
      out[name]    = material;
    });
    return out;
  }

  // Create a new cube grid from a beat grid
  function cubeGridFromBeatGrid(size, padding, beatGrid) {
    let {steps, tracks} = beatGrid;
    let trackCount      = tracks.length;

    let trackMetas = tracks.map(function(track, i) {
      return {
        name     : track.name,
        drawLine : track.type === "curve",
        drawCubes: track.type === "gate"
      };
    });

    let grid    = makeCubeGrid(trackCount, steps, size, padding, trackMetas);
    // create values
    grid.values = mapGridIndices(function(row, col, i) {
      let track = beatGrid.tracks[row];
      return track.values[col % track.steps];
    }, trackCount, steps);

    grid = updateCubeGridWithValuesAndPhase(grid, {
      gridPhase   : 0,
      lastSchedule: {start: 0, end: 0}
    }, true);

    return grid;
  }

  // ## Drawing a graph

  function createValueLine(cubeGrid, values) {
    let {cols, rows, cellSize: {cellSize}, rowMetas} = cubeGrid;

    let curves    = [];
    curves.length = rows;

    let valueLines    = [];
    valueLines.length = rows;

    for (let rowIdx = 0; rowIdx < rows; ++rowIdx) {

      let rowMeta = rowMetas[rowIdx];
      if (!rowMeta.drawLine) continue;

      let yScaleFactor = cellSize * (cols / 4);

      let curvePath = new THREE.CurvePath();
      let rowX      = (rowIdx + 0.5) * cellSize;
      let lastPoint = new THREE.Vector3(rowX, cellSize, 0);

      for (let colIdx = 0; colIdx < cols; ++colIdx) {
        let colZ       = cellSize * colIdx;
        let valueIdx   = rowIdx * cols + colIdx;
        let valueY     = values[valueIdx] * yScaleFactor;
        let valuePoint = new THREE.Vector3(rowX, valueY, colZ);
        if (lastPoint) {

          let midPoint = lastPoint.lerp(valuePoint, 0.9);
          let curve    = new THREE.QuadraticBezierCurve3(lastPoint, midPoint,
                                                         valuePoint);
          curvePath.add(curve);
        } //else {
        lastPoint = valuePoint;
        //}
      }

      curves[rowIdx] = curvePath;

    }

    return {
      mesh    : valueLines,
      curves  : curves,
      extruded: curves.map(
          function(curve) { return createValueMesh(curve, cellSize); })
    };
  }

  function createValueMesh(curvePath, cellSize) {
    let length = cellSize * 0.8, width = cellSize * 0.05;

    let shape = new THREE.Shape();
    shape.moveTo(0, 0);
    shape.lineTo(0, width);
    shape.lineTo(length, width);
    shape.lineTo(length, 0);
    //shape.lineTo(0, 0);

    let extrudeSettings = {
      steps        : curvePath.curves.length * 4,
      amount       : cellSize,
      bevelEnabled : false,
      extrudePath  : curvePath,
      //bevelThickness: width * 0.1,
      //bevelSize     : width,
      bevelSegments: 3
    };

    let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
    //let material = new THREE.MeshBasicMaterial({color: 0x00ff00});
    let material = new THREE.MeshNormalMaterial();
    let mesh     = new THREE.Mesh(geometry, material);
    return mesh;
  }

  // ## Cubic stuff

  // Do some fn for each grid index in a rows x cols grid.
  //
  // If out is provided, the that will be used as output
  function mapGridIndices(fn, rows, cols, out = []) {
    out.length = rows * cols;

    for (let row = 0; row < rows; ++row) {
      for (let col = 0; col < cols; ++col) {
        out[row * cols + col] = fn(row, col, row * cols + col);
      }
    }
    return out;
  }

  function _createBgCubes(cols, cellSize, rows) {
    // create the materials for the bg cubes
    let bgCubeRowMaterials = [];
    for (let col = 0; col < cols; ++col) {
      bgCubeRowMaterials.push(new THREE.MeshBasicMaterial({color: 0x222233}));
    }

    let bgCubeFactor   = {
      x: 0.90,
      y: 0.01,
      z: 0.90
    };
    let bgCubeGeometry = new THREE.BoxGeometry(
        cellSize * bgCubeFactor.x,
        cellSize * bgCubeFactor.y,
        cellSize * bgCubeFactor.z
    );

    let bgCubes = mapGridIndices(function(row, col, i) {
      let material    = bgCubeRowMaterials[col];
      //let material    = new THREE.MeshBasicMaterial({color: 0x222233});
      let cube        = new THREE.Mesh(bgCubeGeometry, material);
      cube.position.x = row * cellSize;
      cube.position.z = col * cellSize;
      cube.position.y = -1 * cellSize * (bgCubeFactor.y / 2 + 1);

      return cube;
    }, rows, cols);
    return {
      bgCubeRowMaterials,
      bgCubes
    };
  }

  // Creates a new, cube grid with all values set to zero
  function makeCubeGrid(rows, cols, size, padding, rowMetas) {
    let cellSize = size + (padding * 2);
    let geometry = new THREE.BoxGeometry(size, size, size);

    function beatCube(size, row, col) {
      let material    = new THREE.MeshBasicMaterial({color: 0x00ff00});
      let cube        = new THREE.Mesh(geometry, material);
      cube.position.x = row * cellSize + padding;
      cube.position.z = col * cellSize + padding;

      return cube;
    }

    // create cubes
    let cubes = mapGridIndices(function(row, col, i) {
      return beatCube(size, row, col);
    }, rows, cols);

    // create values
    let values                        = mapGridIndices(function(row, col, i) {
      return 0.0;
    }, rows, cols);
    let {bgCubeRowMaterials, bgCubes} = _createBgCubes(cols, cellSize, rows);
    //let cursorCube =

    let gridCellSize = {
      cubeSize: size,
      padding : padding,
      cellSize: cellSize
    };

    function makeCursorCube(size = 0.9, vSize = 0.1, color = 0xcccccc) {
      let material    = new THREE.MeshBasicMaterial({color});
      let boxGeometry = new THREE.BoxGeometry(rows * cellSize, size *
          cellSize * vSize, size * cellSize);
      let mesh        = new THREE.Mesh(boxGeometry, material);
      mesh.position.x = (rows - 1) * cellSize / 2;
      mesh.position.y = -((vSize + 1) * cellSize );
      return {
        color,
        size,
        mesh
      };
    }

    let colors = {
      inPhase : [
        0x82042F,
        0xeeeeee
      ],
      offPhase: [
        0x82042F,
        0xC60886
      ]
    };
    return {
      rows,
      cols,
      cubes,
      values,
      phase: 0,

      cellSize: gridCellSize,
      colors,

      cursorCubeSize: 0.2,
      cursorCube    : makeCursorCube(),

      rowMetas,
      bgCubes,
      bgCubeRowMaterials
    };
  }

  function getCubeScale(
      minScale, maxScale, isActive, phaseDist, phaseStep, value)
  {
    let phaseScale = maxScale +
        ( isActive
            ? (Math.cos((phaseDist / phaseStep - 0.02) * Math.PI / 2))
            : -(maxScale - minScale));

    let scaleValue = value < 0.01 ? 0.1 : THREE.Math.lerp(0.3, 1.0, value);
    return new THREE.Vector3(
        phaseScale * scaleValue,
        phaseScale * scaleValue,
        THREE.Math.lerp(0.7, 1.0, value));
  }

  function _updateBgCubeMaterials(cubeGrid, cols, activeColumn) {
    let bgCubeMaterials = cubeGrid.bgCubeRowMaterials;
    for (let col = 0; col < cols; ++col) {
      let color;
      if (activeColumn === col) {
        color = 0x60606B;
      } else {
        color = 0x202029;
      }

      bgCubeMaterials[col].color.setHex(color);
    }
  }

  function updateCubeGridWithValuesAndPhase(cubeGrid, player, force = false) {
    let {
          rows, cols, cubes, colors, cellSize, cursorCubeSize, values
        } = cubeGrid;

    let phase = player.gridPhase % 1.0;

    let phaseStep = 1.0 / cubeGrid.cols;

    let activeColumn = Math.floor(phase / phaseStep);
    //let activeColumn = Math.floor(phase / phaseStep);
    //let inactiveColor = new THREE.Color(0x);

    mapGridIndices(function(row, col, i) {
      let cube     = cubes[i];
      let value    = values[i];
      let hasValue = value > 0.001;

      // skip unless forced to
      //if (!hasValue && !force) return;

      let phaseDist = phase - (col * phaseStep);
      let isActive  = col === activeColumn;

      let valueColors = colors[isActive ? "inPhase" : "offPhase"];
      let colorValues = valueColors.map(
          function(c) {return new THREE.Color(c);});

      let color = colorValues[0].lerp(colorValues[1], value);

      cube.material.color.set(color);

      let minScale  = 0.9;
      let maxScale  = 1.0;
      let cubeScale = getCubeScale(minScale, maxScale, isActive, phaseDist,
                                   phaseStep,
                                   value
      );
      cube.scale.x  = cubeScale.x;
      cube.scale.y  = cubeScale.y;
      cube.scale.z  = cubeScale.z;

    }, rows, cols);
    _updateBgCubeMaterials(cubeGrid, cols, activeColumn);

    if (!cubeGrid.valueLines) {
      cubeGrid.valueLines = createValueLine(cubeGrid, values);
    }

    cubeGrid.values = values;
    cubeGrid.phase  = phase;

    cubeGrid.cursorCube.mesh.position.z = (player.lastSchedule.start % 1.0) *
        cols *
        cellSize.cellSize;
    return cubeGrid;
  }

  function updateCubeGridPhase(cubeGrid, player, force = false) {
    return updateCubeGridWithValuesAndPhase(cubeGrid, player, force);
  }

  function cameraTargetForCubeGrid(cubeGrid) {
    let {rows, cols, cellSize, phase} = cubeGrid;
    return new THREE.Vector3(
        (rows * cellSize.cellSize) / 2,
        0,
        (cols * cellSize.cellSize) * phase
    );

  }

  // add cubes to the scene
  function addCubeGridToScene(cubeGrid, scene) {
    let {cols, rows, cubes, cursorCube, rowMetas, bgCubes} = cubeGrid;

    for (let row = 0; row < rows; ++row) {
      if (rowMetas[row].drawLine) continue;
      for (let col = 0; col < cols; ++col) {
        let cubeIdx = row * cols + col;
        scene.add(cubes[cubeIdx]);
      }
    }

    for (let row = 0; row < rows; ++row) {
      for (let col = 0; col < cols; ++col) {
        let cubeIdx = row * cols + col;
        scene.add(bgCubes[cubeIdx]);
      }
    }

    //scene.add(cursorCube.mesh);

    cubeGrid.valueLines.extruded.forEach(function(l, i) {
      scene.add(l);
    });
  }

  return {
    createEmpty : makeCubeGrid,
    fromBeatGrid: cubeGridFromBeatGrid,

    addToScene: addCubeGridToScene,

    updatePhase                     : updateCubeGridPhase,
    updateCubeGridWithValuesAndPhase: updateCubeGridWithValuesAndPhase,
    cameraTargetFor                 : cameraTargetForCubeGrid
  };

}();
