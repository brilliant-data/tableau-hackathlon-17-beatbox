window.onload = init;
var context;
var bufferLoader;

function init() {
  // Fix up prefixing
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  context = new AudioContext();

  bufferLoader = new BufferLoader(
    context,

    SOUNDS,
    RhythmSample.play
  )

  bufferLoader.load();
}


var SOUNDS = [
  "sounds/BBD-claps-mac-clap-10.wav",
  "sounds/x.wav",
];

var BUFFERS = [
];


var RhythmSample = {
};

RhythmSample.play = function(BUFFERS) {
  function playSound(buffer, time) {
    var source = context.createBufferSource();
    source.buffer = buffer;
    source.connect(context.destination);
    if (!source.start)
      source.start = source.noteOn;

    console.log("playing at ", time )
    source.start(time);
  }

  var kick = BUFFERS[0] //.kick;
  // var snare = BUFFERS.snare;
  // var hihat = BUFFERS.hihat;

  // We'll start playing the rhythm 100 milliseconds from "now"
  var startTime = context.currentTime + 0.500;
  var tempo = 80; // BPM (beats per minute)
  var eighthNoteTime = (60 / tempo) / 2;

  // Play 2 bars of the following:
  for (var bar = 0; bar < 4; bar++) {
    var time = startTime + bar * 8 * eighthNoteTime;
    // Play the bass (kick) drum on beats 1, 5
    playSound(kick, time);
    playSound(kick, time + 4 * eighthNoteTime);

    // Play the snare drum on beats 3, 7
    // playSound(snare, time + 2 * eighthNoteTime);
    // playSound(snare, time + 6 * eighthNoteTime);

    // Play the hi-hat every eighthh note.
    // for (var i = 0; i < 8; ++i) {
      // playSound(hihat, time + i * eighthNoteTime);
    // }
  }
};


function start() {
  init()
}
