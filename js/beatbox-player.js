"use strict";

let beatboxPlayer = function() {
  // creates and returns a new player state
  function init() {

    // create the audio context
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    let context         = new AudioContext();
    // instantiate a loader
    let loader          = new THREE.AudioLoader();

    let mainOut = context.destination;

    // create the master output
    let compressor             = context.createDynamicsCompressor();
    compressor.threshold.value = -12;
    compressor.knee.value      = 0;
    compressor.ratio.value     = 2.0;
    compressor.attack.value    = 0.01;
    compressor.release.value   = 0.17;

	
    compressor.connect(mainOut);
	mainOut = compressor;

    return _resetPlayer({
                          context,
                          isPlaying     : false,
                          startedAt     : 0,
                          beatGrid      : null,
                          scheduledUntil: 0,
                          loader        : loader,
                          loadedFiles   : {},
                          _activeLoads  : {},
                          gridPhase     : 0,
                          lastSchedule  : {start: 0, end: 0},

                          _mainOut: mainOut
                        });
  }

  // starts the playback of the given beatgrid
  function startPlayback(player, beatGrid) {
    // fetch the start time
    let startedAt = player.context.currentTime;
    // add some offset for the playback start
    startedAt += 0.2;

    player.startedAt             = startedAt;
    player.scheduledUntil        = 0;
    player.tracksSecheduledUntil = {};
    player.isPlaying             = true;

    return player;
  }

  function _resetPlayer(player) {
    player.startedAt             = 0;
    player.scheduledUntil        = 0;
    player.tracksSecheduledUntil = {};
    player.gridPhase             = 0;
    player.isPlaying             = false;
    return player;
  }

  // stops the playback
  function stopPlayback(player) {
    return _resetPlayer(player);
  }

  // ## Loading sounds

  // Loads the given sound file to the player
  function loadSound(player, file) {
    if (player.loadedFiles[file]) {
      console.log("Skipping duplicate load: already loaded sound: %o", file);
      return;
    }
    if (player._activeLoads[file]) {
      console.log("Skipping duplicate load: sound load already in progress: %o",
                  file);
      return;
    }

    player._activeLoads[file] = true;
    // load a resource
    player.loader.load(
        // resource URL
        file,
        // Function when resource is loaded
        function(audioBuffer) {
          player._activeLoads[file] = false;
          player.loadedFiles[file]  = audioBuffer;
        },
        // Function called when download progresses
        function(xhr) {
          console.debug("[LOAD PROGRESS] %o : %d bytes / %d bytes [ %d ]", file,
                        xhr.loaded,
                        xhr.total, (xhr.loaded / xhr.total * 100));
        },
        // Function called when download errors
        function(xhr) {
          player._activeLoads[file] = false;
          console.log("An error happened");
        }
    );
  }

  function loadSoundsForMapping(player, soundMapping) {
    console.log(player, soundMapping);
    Object.keys(soundMapping)
          .forEach(track => loadSound(player, soundMapping[track].file));
  }

  function _getEventsForTrack(stepLen, stepCount, track, trackIdx)
  {
    let {name, values, steps, type} = track;
    let events                      = [];
    // create all events
    for (let i = 0; i < stepCount; ++i) {
      let t = stepLen * i;
      let v = values[i % steps];
      events.push({
                    name,
                    atTime    : t,
                    value     : v,
                    index     : i,
                    trackIdx  : trackIdx,
                    localIndex: i % steps
                  });
    }

    return events;
  }

  function _addModulationValuesForTrack(
      stepLen, stepCount, track, valuesByTrackAndTargetDict)
  {
    let {forTrack, targets, amount, values, steps} = track;
    // the values need to be resampled
    let vs                                         = [];
    vs.length                                      = stepCount;

    // create all events
    for (let i = 0; i < stepCount; ++i) {
      let atTime = stepLen * i;
      let value  = values[i % steps];
      vs[i]      = value * amount;
    }

    targets.forEach(target => {
      let key   = `${forTrack} / ${target}`;
      let cache = valuesByTrackAndTargetDict[target];

      if (!cache) {
        cache        = [];
        cache.length = stepCount;

        for (let i = 0; i < stepCount; ++i) {
          cache[i] = 0;
        }
      }

      for (let i = 0; i < stepCount; ++i) {
        cache[i] += vs[i];
      }

      valuesByTrackAndTargetDict[target] = cache;
    });

  }

  function _getEventsForGrid(grid) {
    let {bpm, stepsPerBeat, steps, tracks} = grid;

    // step length in milliseconds
    let stepLen = stepLengthTime(bpm, stepsPerBeat, 1);

    let trackEvents = tracks.filter(t => t.type === "gate").map(
        (track, i) => _getEventsForTrack(stepLen, steps, track, i));

    let modulationDict = {};

    tracks.filter(t => t.type === "gate")
          .forEach(t => modulationDict[t.name] = {});

    tracks.filter(t => t.type === "curve")
          .forEach(
              track => {
                _addModulationValuesForTrack(stepLen, steps, track,
                                             modulationDict[track.forTrack]);
              });

    // weave modulation events into track events
    let events = Array.prototype.concat.apply([], trackEvents)
                      .filter(e => e.value > 0)

                      .map(evt => {
                        evt.modulators = modulationDict[evt.name];
                        return evt;
                      });

    return {
      duration: stepLen * steps,
      events
    };
  }

  // Offsets all events in an event list by timeOffset
  function _offsetEvents(timeOffset, events) {
    return events.map(e => Object.assign(e, {atTime: e.atTime + timeOffset}));
  }

  // Playes a sound using the context
  function _playSound(context, sound, buffer, event, voice)
  {
    let {value, modulators, index} = event;
    let time                       = event.atTime;
    //let source                     = context.createBufferSource();
    //source.buffer                  = buffer;
    //let dst                        = source;

    sound = Object.assign({}, sound);
    // apply modulation scaling
    Object.keys(modulators).forEach(param => {
      sound[param] += modulators[param][index];
    });

    let {start, length, playbackRate, gain, highpass, lowpass, valueTo} = sound;
    //if (modulators.playbackRate) {
    //  playbackRate += modulators.playbackRate[index];
    //}

    function scaledWithValue(base, scale, value) {
      return base + (value - 0.5) * 2 * (scale || 0);
    }

    // apply gain
    if (gain) {
      //let gainNode        = context.createGain();
      //let dbGain          = scaledWithValue(gain, valueTo.gain, value);
      // convert back the decibels
      if (!voice || !voice.gain) {
        throw new Error("SADASD");
      }
      voice.gain.gain.value = Math.pow(10, scaledWithValue(gain, valueTo.gain,
                                                           value) / 20);
      //dst                 = dst.connect(gainNode);
    }

    // apply highpass
    if (highpass) {
      voice.highpass.frequency.value = scaledWithValue(highpass,
                                                       valueTo.highpass,
                                                       value);
    }

    // apply highpass
    if (lowpass) {
      voice.lowpass.frequency.value = scaledWithValue(lowpass, valueTo.lowpass,
                                                      value);
    }

    if (!voice.source.start) {
      voice.source.start = voice.source.noteOn;
    }

    voice.source.playbackRate.value = playbackRate;
    voice.source.start(time, start * buffer.duration * start, buffer.duration *
        length);

  }

  // Creates a new playback voice for each step in a sound mapping
  function _createVoicesFor(
      n, soundMapping, player, connectOutputTo, trackName)
  {
    if (!soundMapping) return null;

    soundMapping = Object.assign({
                                   file        : null,
                                   gain        : -6,
                                   playbackRate: 1.0,
                                   lowpass     : 20000,
                                   highpass    : 40
                                 }, soundMapping);

    let buffer = player.loadedFiles[soundMapping.file];
    if (!buffer) {
      if (player._activeLoads[soundMapping.file]) {
        return null;
      }
      throw new Error("Sound file '" + soundMapping.file + "' not loaded");
    }

    let context        = player.context;
    let hp             = context.createBiquadFilter();
    hp.type            = "highpass";
    hp.frequency.value = soundMapping.highpass;

    let lp             = context.createBiquadFilter();
    lp.type            = "lowpass";
    lp.frequency.value = soundMapping.lowpass;

    let gain        = context.createGain();
    gain.gain.value = 1.0;

    hp.connect(lp).connect(gain).connect(connectOutputTo);
    let channel = hp;
    let sources = [];
    //
    for (let i = 0; i < n; ++i) {
      // per-voice gain
      let stepGain        = context.createGain();
      stepGain.gain.value = 1.0;
      let gain            = context.createGain();
      gain.gain.value     = soundMapping.gain;
      sources.push({
                     source  : null,
                     gain    : gain,
                     stepGain: stepGain,
                     lowpass : lp,
                     highpass: hp
                   });
      //
      gain.connect(stepGain).connect(channel);
    }

    return {
      file    : soundMapping.file,
      buffer  : buffer,
      lowpass : lp,
      highpass: hp,
      gain    : gain,
      channel : channel,
      sources : sources,
      track   : trackName
    };

  }

  // Creates a new voice source pack for each loop iteration
  function _createSourcesForVoice(context, voice, n) {

    if (!voice) {
      return null;
    }

    for (let i = 0; i < n; ++i) {

      let source = context.createBufferSource();

      source.buffer = voice.buffer;
      if (voice.sources[i].source) {
        //voice.sources[i].source.disconnect()
        //delete voice.sources[i].source;
      }
      voice.sources[i].source = source;

      source.connect(voice.sources[i].gain);
    }

    return voice;

  }

  function _updateVoice(voice, {highpass, lowpass, gain}) {
    if (highpass) { voice.highpass.frequency.value = highpass; }
    if (lowpass) { voice.lowpass.frequency.value = lowpass; }

    if (gain) { voice.gain.gain.value = Math.pow(10, gain / 20); }
  }

  function _playEvents(player, soundMapping, events, voices) {

    events.forEach(function(evt) {
      let {name, value, localIndex, index, atTime, trackIdx} = evt;
      let mappedSound                                        = soundMapping[name];
      if (!mappedSound) { return; }

      let buffer = player.loadedFiles[mappedSound.file];
      if (!buffer) { return; }

      let voice = voices[trackIdx];
      if (!voice) return;
      let source = voice.sources[index];

      _playSound(player.context, mappedSound, buffer, evt, source);
    });
  }

  // Plays a loop once
  function playLoop(startTime, soundMapping, grid, player) {
    let gridEvents   = _getEventsForGrid(grid);
    let eventsOffset = _offsetEvents(startTime, gridEvents.events);

    let voices = player._voices;
    if (!voices) {
      voices = grid.tracks.map(
          track => _createVoicesFor(grid.steps,
                                    soundMapping[track.name],
                                    player,
                                    player._mainOut,
                                    track.name
          )
      );
      if (voices.filter(v => v !== null).length === 0) {
        return gridEvents.duration;
      }
      player._voices = voices;
    }

    voices = grid.tracks.filter(t => t.type === "gate")
                 .map((track, i) =>
                          _createSourcesForVoice(player.context, voices[i],
                                                 grid.steps));
    _playEvents(player, soundMapping, eventsOffset, voices);
    return gridEvents.duration;
  }

  function _logToHeader(msg) {
    let el       = document.getElementById("log");
    el.innerHTML = (msg + " | " + el.innerHTML).substr(0, 150);
  }

  function tickLoop(lookAhead, soundMapping, grid, player) {
    if (!player.isPlaying) {
      return;
    }

    let currentTime = player.context.currentTime;
    let triggerTime = currentTime + lookAhead;
    let nextLoop    = player._nextLoop || triggerTime;
    let duration    = player._duration || 9999;
    let timeout     = nextLoop + 12;

    let shouldTrigger = (currentTime < nextLoop) && (triggerTime >= nextLoop);
    if (currentTime > timeout) {
      _resetPlayer(player);
    }

    if (shouldTrigger) {
      duration         = playLoop(nextLoop, soundMapping, grid, player);
      nextLoop         = nextLoop + duration;
      //console.log("------")
      player._nextLoop = nextLoop;
      player._duration = duration;
    }

    let gridPhase = 1.0 - ((nextLoop - currentTime) / duration);

    while (gridPhase < 0) gridPhase += 1;
    player.gridPhase = gridPhase;

    //console.log(nextLoop, currentTime, duration, player.gridPhase)
    //_logToHeader(Math.ceil(player.gridPhase * grid.steps).toString());

  }

  // Returns v of the next cycle staart (like align for memory).
  function _alignToCycle(v, cycle) {
    return Math.ceil(v / cycle) * cycle;
  }

  function _dbToLinear(db) { return Math.pow(10, db / 20); }

  function setGainFor(gainInDbMap, player) {

    let voices = player._voices;

    function _voiceForTrack(trackName, voices) {
      for (let i = 0; i < voices.length; ++i) {
        if (voices[i].track === trackName) return voices[i];

      }

      throw new Error("Cannot find track for name:" + trackName);
      //return null;
    }

    Object.keys(gainInDbMap)
          .forEach(track => {
            let voice = _voiceForTrack(track, voices);
            voice.gain.gain.value = _dbToLinear(gainInDbMap[track]);
          });
  }

  function setStepGain(stepIdx, gainDb, player) {

    let voices = player._voices;

    voices.forEach(voice => {
                     if (!voice) return;
                     let source                 = voice.sources[stepIdx];
                     source.stepGain.gain.value = _dbToLinear(gainDb);
                   }
    );

  }

  //function _scheduleTrack(
  //    scheduleStart, scheduleEnd, stepLen, gridSteps, track, playheadTime,
  //    trackState, grid)
  //{
  //
  //  // only gate tracks emit actual events
  //  if (track.type !== "gate") {
  //    return {
  //      events        : [],
  //      scheduledUntil: scheduleEnd
  //    };
  //  }
  //
  //  let {values, name, type} = track;
  //  let steps                = Math.min(track.steps, gridSteps);
  //  let events               = [];
  //
  //  // the start time of the next loop iteration
  //  // Step through
  //  let startPos = _alignToCycle(scheduleStart, stepLen);
  //  let t        = startPos;
  //
  //  let trackLen = stepLen * steps;
  //
  //  while (true) {
  //    // play the stuff here
  //
  //    if (t >= scheduleEnd) {
  //      return {
  //        events        : events,
  //        scheduledUntil: scheduleEnd
  //
  //      };
  //    }
  //
  //    let trackPhase = (t % trackLen) / trackLen;
  //    let valueIndex = Math.floor(trackPhase * steps) % steps;
  //    let value      = values[valueIndex];
  //
  //    //if (value !== 0 && t > scheduleStart) {
  //    events.push({
  //                  name,
  //                  valueIndex,
  //                  value,
  //                  atTime: t,
  //                  scheduleStart,
  //                  scheduleEnd,
  //                  startPos
  //                });
  //    //}
  //
  //    // next step
  //    t += stepLen;
  //  }
  //
  //}
  //
  ////
  //function tick(player, beatGrid, options = {})
  //{
  //  // nothing if we arent playing
  //  if (!player.isPlaying) { return player; }
  //
  //  let {windowSize = 0, lookAhead = 0}      = options;
  //  let {startedAt, context, scheduledUntil} = player;
  //  let {bpm, stepsPerBeat, steps}           = beatGrid;
  //
  //  // update clock
  //  let playHeadTime = context.currentTime - startedAt;
  //
  //  // step length in milliseconds
  //  let stepLen = stepLengthTime(bpm, stepsPerBeat, 1);
  //  let gridLen = stepLen * steps;
  //  // the current step in the grid
  //  //console.log(playHeadTime);
  //
  //  // schedule window size.
  //  // If we scheduled anything before, then clamp
  //  let baseScheduleStart = playHeadTime + lookAhead;
  //  let scheduleStart     = Math.max(baseScheduleStart, scheduledUntil);
  //
  //  let gridPhase = (gridLen + scheduleStart) % gridLen;
  //  let gridStep  = (steps + Math.floor(gridPhase * steps)) % steps;
  //
  //  // check the end using the regular window size
  //  let scheduleEnd = baseScheduleStart + windowSize;
  //  if (scheduleEnd < scheduledUntil) {
  //    // already scheduled everything
  //    return player;
  //  }
  //
  //  // collect the schedules
  //  let events = [];
  //
  //  // for each track find stuff to schedule
  //  beatGrid.tracks.forEach(function(track, trackIndex) {
  //    let scheduledTrackUntil = player.tracksSecheduledUntil[track.name] ||
  //        baseScheduleStart;
  //
  //    if (scheduledTrackUntil >= scheduleEnd) { return; }
  //
  //    let res = getEventsInWindow(track.name, track.values, track.steps,
  //                                stepLen, scheduledTrackUntil, windowSize);
  //
  //    //let res = _scheduleTrack(scheduledTrackUntil, scheduleEnd, stepLen,
  //    // steps, track, playHeadTime, track.state || {lastTick:0}, beatGrid);
  //
  //    player.tracksSecheduledUntil[track.name] = res.scheduledUntil;
  //    events.push(...res.events);
  //
  //  });
  //
  //  function playSound(
  //      {file, start, length, playbackRate, gain, highpass, lowpass,
  // valueTo},
  //      buffer,
  //      time, value)
  //  {
  //    let source    = context.createBufferSource();
  //    source.buffer = buffer;
  //
  //    let dst = source;
  //
  //    function scaledWithValue(base, scale, value) {
  //      return base + (value - 0.5) * 2 * (scale || 0);
  //
  //    }
  //
  //    // apply gain
  //    if (gain) {
  //      let gainNode        = context.createGain();
  //      let dbGain          = scaledWithValue(gain, valueTo.gain, value);
  //      // convert back the decibels
  //      gainNode.gain.value = Math.pow(10, dbGain / 20);
  //      dst                 = dst.connect(gainNode);
  //    }
  //
  //    // apply highpass
  //    if (highpass) {
  //      let filterNode             = context.createBiquadFilter();
  //      filterNode.type            = "highpass";
  //      filterNode.frequency.value = scaledWithValue(highpass,
  // valueTo.highpass, value); dst                        =
  // dst.connect(filterNode); }  // apply highpass if (lowpass) { let
  // filterNode             = context.createBiquadFilter(); filterNode.type
  //        = "lowpass"; filterNode.frequency.value = scaledWithValue(lowpass,
  // valueTo.lowpass, value); dst                        =
  // dst.connect(filterNode); }  dst.connect(context.destination);
  // //source.connect(context.destination); if (!source.start) source.start =
  // source.noteOn;  //source.duration = buffer.duration * 0.01;
  // source.playbackRate.value = playbackRate;  console.log("%o -> %f, %f",
  // file, time / stepLen, time / stepLen % steps);  source.start(time, start *
  // buffer.duration * start, buffer.duration * length); }  let
  // trackSoundMapping = { "Kick": { //file  : "sounds/loop01.wav", file
  // : "sounds/BBD-claps-mac-clap-10.wav", start       : 0, length      : 0.05,
  // playbackRate: 0.5, gain        : -24, lowpass     : 2000,  valueTo: { gain
  //   : 12, lowpass: 2000 } } //"Snare": { //  file        :
  // "sounds/BBD-claps-mac-clap-10.wav", //  start       : 0, //  length      :
  // 0.2, //  playbackRate: 1.0, //  gain        : 0.4, //  highpass    : 500,
  // // //  valueTo: { //    gain: 0.1 //  } //} };  if (events.length > 0)
  // console.log("---");  events.filter(function(e) {return e.value >
  // 0;}).forEach(function(evt) { let {name, value, valueIndex, atTime} = evt;
  // let mappedSound                       = trackSoundMapping[name]; if
  // (!mappedSound) { return; } //console.log("[ %f ]", atTime, mappedSound);
  // let buffer = player.loadedFiles[mappedSound.file]; if (!buffer) { return;
  // }  //console.log(evt) playSound(mappedSound, buffer, atTime, value); //if
  // (valueIndex === atTime / stepLen % steps) { //  playSound(mappedSound,
  // buffer, atTime, value); //  console.log("at", atTime) //} else { //  throw
  // new Error("Mistimed") //}  }); // update the schedule endpoint
  // //player.scheduledUntil = scheduleEnd; player.scheduledUntil =
  // scheduledUntil; player.activeStep     = gridStep; player.gridPhase      =
  // gridPhase;  player.lastSchedule = {start: scheduleStart, end:
  // scheduleEnd};  // schedule return player; }

  return {
    init,
    start               : startPlayback,
    stop                : stopPlayback,
    loadSound,
    loadSoundsForMapping: loadSoundsForMapping,
    playLoop            : playLoop,
    tickLoop            : tickLoop,
    setGainFor          : setGainFor,
    setStepGain         : setStepGain
  };

}();