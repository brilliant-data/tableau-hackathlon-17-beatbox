"use strict";

// ## Beat grid

function stepLengthTime(bpm, stepsPerBeat, timeUnitsInSeconds = 1000) {
  let stepsPerMinutes = bpm * stepsPerBeat;
  return (timeUnitsInSeconds * 60) / stepsPerMinutes;
}

const RUNTIME_LIMIT = 3000;

let TRACK_SOUND_MAPPING = {
  "Kick"      : {
    //file  : "sounds/loop01.wav",
    file        : "beatbox/sounds/Kick/Kick 003 Sensible.wav",
    start       : 0,
    length      : 0.4,
    playbackRate: 0.9,
    gain        : -24,
    lowpass     : 7000,

    valueTo: {
      gain   : 12,
      lowpass: 2000
    }
  },
  "Snare"     : {
    file        : "beatbox/sounds/snares/Clap 002.wav",
    start       : 0,
    length      : 0.2,
    playbackRate: 1.0,
    gain        : -36,
    highpass    : 200,

    valueTo: {
      gain: 12
    }
  },
  "Closed Hat": {
    file        : "beatbox/sounds/hats/HiHat Closed 001 Widened.wav",
    start       : 0,
    length      : 1.0,
    playbackRate: 1,
    gain        : -12,
    highpass    : 1000,
    lowpass     : 7000,

    valueTo: {
      gain   : 12,
      lowpass: 4000
    }
  },
  "Bass"      : {
    file        : "beatbox/sounds/Instruments/Orchestra Hit 001 Revved.wav",
    start       : 0,
    length      : 0.4,
    playbackRate: 0.6,
    gain        : -28,
    highpass    : 600,
    lowpass     : 1500,

    valueTo: {
      gain   : 12,
      lowpass: 2000
    }
  },
  "Bass2"     : {
    file        : "beatbox/sounds/Instruments/Orchestra Hit 001 Revved.wav",
    start       : 0,
    length      : 0.1,
    playbackRate: 0.8,
    gain        : -28,
    highpass    : 600,
    lowpass     : 1500,

    valueTo: {
      gain   : 12,
      lowpass: 2000
    }
  },
  "fx"        : {
    file        : "beatbox/sounds/Instruments/Timpani 001 Revved.wav",
    start       : 0,
    length      : 0.2,
    playbackRate: 0.5,
    gain        : -18,
    highpass    : 900,
    lowpass     : 4000,

    valueTo: {
      gain   : 12,
      lowpass: 1000
    }
  },
  "fx2"       : {
    file        : "beatbox/sounds/Instruments/Timpani 001 Revved.wav",
    start       : 0,
    length      : 0.1,
    playbackRate: 1.5,
    gain        : -32,
    highpass    : 1200,
    lowpass     : 2000,

    valueTo: {
      gain   : 12,
      lowpass: 2000
    }
  }
};

const SAMPLE_BEATGRID = {
  steps       : 16,
  bpm         : 92,
  stepsPerBeat: 4,
  tracks      : [
    {
      name  : "Kick",
      type  : "gate",
      steps : 16,
      values: [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0.9]
    },
    {
      name  : "Snare",
      type  : "gate",
      steps : 16,
      values: [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0.2, 0]
    },
    {
      name  : "Closed Hat",
      type  : "gate",
      steps : 4,
      values: [
        0.5, 0.2, 1, 0.2
        , 0.5, 0.2, 1, 0.2
        , 0.5, 0.2, 1, 0.2
        , 0.5, 0.2, 1, 0.2
      ]
    },
    {
      name  : "Shaker",
      type  : "gate",
      steps : 4,
      values: [
        0, 0, 1, 0.2
        , 0, 0, 1, 0.2
        , 0, 0, 1, 0.2
        , 0, 0, 1, 0.2
      ]
    },
    //
    {
      name  : "Bass",
      type  : "gate",
      steps : 16,
      values: [1, 0, 0, 0.4, 0, 0, 0, 0, 0, 0, 1, 0, 0.9, 0, 0, 0.5]
    },

    //
    {
      name  : "Bass2",
      type  : "gate",
      steps : 16,
      values: [0.7, 0, 0, 0, 0, 0, 0.3, 0.3, 0, 0, 0, 0, 0, 0, 0, 0]
    },
    {
      name  : "fx",
      type  : "gate",
      steps : 16,
      values: [0, 0.1, 0, 0, 0, 0, 0.2, 0, 0, 0.6, 0, 0, 0, 0.1, 0, 0]
    },

    //
    {
      name  : "fx2",
      type  : "gate",
      steps : 16,
      values: [0, 0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0, 0.8]
    },
    {
      name    : "Hat pitch",
      type    : "curve",
      steps   : 16,
      forTrack: "Bass",
      targets : ["playbackRate"],
      amount  : 0.5,
      values  : [
        0,
        0.4,
        1,
        0.5,
        0.5,
        0.5,
        0.5,
        0.5,
        0.5,
        1,
        1,
        1,
        0.9,
        0.8,
        0.7,
        0.6]
    },
    {
      name    : "Hat len",
      type    : "curve",
      steps   : 16,
      forTrack: "Closed Hat",
      targets : ["length"],
      amount  : 0.01,
      values  : [
        0.5,
        0.5,
        0.5,
        0.5,
        0.5,
        0.5,
        0.5,
        0.5,
        0,
        0,
        1,
        1,
        0.5,
        0.5,
        0.5,
        0.5]
    },
    {
      name    : "Snare len",
      type    : "curve",
      steps   : 16,
      forTrack: "Snare",
      targets : ["playbackRate"],
      amount  : 0.5,
      values  : [
        0.5,
        0.5,
        0.5,
        0.5,
        1,
        0.5,
        0.5,
        0.5,
        0,
        0,
        1,
        1,
        0,
        0.5,
        0.5,
        0.5]
    }
  ]
};

let tabHackBeatbox = function() {

      let container;
      let camera, scene, renderer, composerInfo;
      let player = beatboxPlayer.init();

      beatboxPlayer.start(player, SAMPLE_BEATGRID);
      beatboxPlayer.loadSoundsForMapping(player, TRACK_SOUND_MAPPING);

      let GRID = {
        size   : 0.95,
        padding: 0.05
      };

      let cubeGrid = stepGrid.fromBeatGrid(GRID.size, GRID.padding,
                                           SAMPLE_BEATGRID);

      let controls;
      init();
      requestAnimationFrame(animate);

      function _createComposer(scene, camera, renderer) {
        let composer   = new THREE.EffectComposer(renderer);
        let renderPass = new THREE.RenderPass(scene, camera);

        composer.addPass(renderPass);
        let [badTv, staticNoise, colorShift, film] = _addShadersToCompositor(
            composer, [
              THREE.BadTVShader,
              THREE.StaticShader,
              THREE.RGBShiftShader,
              THREE.FilmShader
            ]);

        return {
          composer: composer,
          passes  : {
            renderPass: renderPass,
            badTv, film, staticNoise, colorShift
          }
        };
      }

      function _addShadersToCompositor(composer, shaders) {
        let passes = shaders.map(s => {
          let pass = new THREE.ShaderPass(s);
          composer.addPass(pass);
          return pass;
        });

        if (passes.length > 0) {
          console.log(passes);
          passes[passes.length - 1].renderToScreen = true;
        }
        return passes;

      }

      //function _addTvCompositor(composer) {
      //  let badTVPass = new THREE.ShaderPass(THREE.BadTVShader);
      //  composer.addPass(badTVPass);
      //  return badTVPass;
      //}
      //
      //function _addFilmCompositor(composer) {
      //  let filmPass = new THREE.ShaderPass(THREE.FilmShader);
      //  composer.addPass(filmPass);
      //  filmPass.renderToScreen = true;
      //  return filmPass;
      //}

      function _addDirectionArrow(scene) {
        //scene.add(new THREE.HemisphereLight(0x443333, 0x222233, 4));

        let dir = new THREE.Vector3(0, 0, 1);

        //normalize the direction vector (convert to vector of length 1)
        dir.normalize();

        let origin = new THREE.Vector3(-1, 0, 0);
        let length = 10;
        let hex    = 0xffff00;

        let arrowHelper = new THREE.ArrowHelper(dir, origin, length, hex);
        scene.add(arrowHelper);
      }

      function init() {
        container = document.createElement("div");
        document.body.appendChild(container);

        //

        camera = new THREE.PerspectiveCamera(35, window.innerWidth /
            window.innerHeight, 0.01, 1000);

        camera.position.x = -11;
        camera.position.z = 5;
        camera.position.y = 10;

        controls        = new THREE.TrackballControls(camera, container);
        controls.target = new THREE.Vector3(5, 0, 8);

        //

        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setClearColor(0x060404);
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight - 32);
        container.appendChild(renderer.domElement);

        scene = new THREE.Scene();

        composerInfo = _createComposer(scene, camera, renderer);

        //_addDirectionArrow(scene);

        stepGrid.addToScene(cubeGrid, scene);

        window.addEventListener("resize", onWindowResize, false);
      }

      function onWindowResize(event) {

        renderer.setSize(window.innerWidth, window.innerHeight - 32);

        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

      }

      // ## Cube grid

      // ## Animation handler

      let runRender = true;

      // register buttons

      function _hook(idHandlerPairs) {

        Object.keys(idHandlerPairs)
              .forEach(id => {

                let handlers = idHandlerPairs[id];
                let el       = document.getElementById(id);

                if (!el) {
                  return;
                  //throw new Error("Cannot find element by id: " + id);
                }

                Object.keys(handlers)
                      .forEach(on => el.addEventListener(on, handlers[on]));

              });
      }

      let {setGainFor, setStepGain} = beatboxPlayer;
      _hook({
              "drums": {
                "input": e => setGainFor({
                                           "Kick"      : e.target.value,
                                           "Closed Hat": e.target.value,
                                           "Snare"     : e.target.value
                                         }, player)
              },
              "bass" : {
                "input": e => setGainFor({
                                           "Bass" : e.target.value,
                                           "Bass2": e.target.value
                                         }, player)
              },
              "fx"   : {
                "input": e => setGainFor({
                                           "fx" : e.target.value,
                                           "fx2": e.target.value
                                         }, player)
              },
              "tempo": {
                "change": e => {
                  SAMPLE_BEATGRID.bpm = e.target.value;
                }
              }

            });

      document.getElementById("render-button")
              .addEventListener("click", function(e) {
                e.preventDefault();
                if (runRender) {
                  runRender          = false;
                  e.target.innerHTML = "START RENDER";
                } else {
                  runRender = true;
                  requestAnimationFrame(animate);
                  e.target.innerHTML = "STOP RENDER";
                }
                return false;
              });

      function _createStepSettersForTrack(
          trackNames, trackIds, stepIds, inElement)
      {

        if (!inElement) {
          return null
          //throw new Error("Cannot insert steps to null element");
        }

        let trackBtns    = trackNames.map((n, i) => {
          return `<button class="select-track" data-track="${i}" >${n}</button>`;
        }).join("\n");
        let trackBtnsDiv = `<span class="track-btns">${trackBtns}</span>`;

        const SCALE_FACTOR = 128;
        let trackControls  = trackIds.map(trackId => {
          let wrapperId = `track-steps-${trackId}`;
          let content   = stepIds.map(stepId => {
            let id   = `track${trackId}-step${stepId}`;
            let html = `<label for="${id}">${stepId}</label>
        <input class="step-value-input" type="range" min="0" max="${SCALE_FACTOR}" value="0" id="${id}" data-track="${trackId}" data-step="${stepId}" />`;
            return html;

          });

          return `<div class="track-steps" id="${wrapperId}"><h5 class="track-label">${trackNames[trackId]}</h5>${content.join(
              "")}</div>`;
        }).join("\n");


        function syncToGrid({tracks}) {
          trackIds.forEach(trackId => {
            let track = tracks[trackId];
            track.values.forEach((val, stepId) => {
              let id      = `track${trackId}-step${stepId}`;
              let control = document.getElementById(id);
              if (!control) return;

              control.value = val;

            });
          });
        }

        // handler for step value changes
        function onTrackClick(e) {
          let trackId      = parseInt(this.dataset.track);
          let newClassName = `show-track-${trackId}`;

          inElement.className = (inElement.className === newClassName)
              ? ""
              : newClassName;

        }

        // handler for step value changes
        function onStepInputChanged(e) {
          let value         = e.target.value / SCALE_FACTOR;
          let {track, step} = this.dataset;
          setStepVolumeAt(parseInt(track), parseInt(step), value);
        }

        inElement.innerHTML = `${trackBtnsDiv} ${trackControls}`;
        // step editor sliders
        document.querySelectorAll(".step-value-input")
                .forEach(e => e.addEventListener("input", onStepInputChanged));

        // track selector buttons
        document.querySelectorAll(".select-track")
                .forEach(e => e.addEventListener("click", onTrackClick));

        return {
          syncToGrid: syncToGrid
        };
      };

      let stepControls = _createStepSettersForTrack(
          SAMPLE_BEATGRID.tracks.map((e, i) => e.name),
          SAMPLE_BEATGRID.tracks.map((e, i) => i),
          //[0, 1, 2, 3],
          //[0, 1, 2, 3, 4, 5, 6],
          [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
          document.getElementById("step-switch-grid")
      );


      if (stepControls) {
        stepControls.syncToGrid(SAMPLE_BEATGRID);
      }

      function animate(time) {

        //return;

        //Check up-front to make sure errors dont stop us
        //if (runRender) {
        //  requestAnimationFrame(animate);
        //}

        let beats   = SAMPLE_BEATGRID;
        let stepLen = stepLengthTime(beats.bpm, beats.stepsPerBeat, 1000);

        cubeGrid         = stepGrid.updatePhase(cubeGrid, player);
        let cameraTarget = stepGrid.cameraTargetFor(cubeGrid);
        //controls.target = cameraTarget;

        beatboxPlayer.tickLoop(0.2, TRACK_SOUND_MAPPING, SAMPLE_BEATGRID, player);

        controls.update();

        let shaderTime                                   = time / 10000;
        composerInfo.passes.badTv.uniforms["time"].value = shaderTime;

        composerInfo.passes.badTv.uniforms["speed"].value       = 3.0;
        composerInfo.passes.badTv.uniforms["rollSpeed"].value   = 0;
        composerInfo.passes.badTv.uniforms["distortion"].value  = 1.0 + 0.2 *
            ((player.gridPhase * beats.steps / beats.stepsPerBeat) % 1.0);
        composerInfo.passes.badTv.uniforms["distortion2"].value = 1.0;

        composerInfo.passes.film.uniforms["time"].value       = shaderTime;
        composerInfo.passes.film.uniforms["grayscale"].value  = 0;
        composerInfo.passes.film.uniforms["sCount"].value     = 800;
        composerInfo.passes.film.uniforms["sIntensity"].value = 0.9;
        composerInfo.passes.film.uniforms["nIntensity"].value = 0.4 + 0.2 *
            ((player.gridPhase * 2 * (beats.steps / beats.stepsPerBeat)) % 1.0);

        composerInfo.passes.colorShift.uniforms["amount"].value = 0.0059 + 0.001 *
            ((player.gridPhase * beats.steps) % 2.13);
        composerInfo.passes.colorShift.uniforms["angle"].value  = 0.95 + 0.1 *
            ((player.gridPhase * beats.steps / beats.stepsPerBeat) % 3.13);

        composerInfo.passes.staticNoise.uniforms["time"].value   = shaderTime;
        composerInfo.passes.staticNoise.uniforms["amount"].value = 0.0099 + 0.05 *
            ((player.gridPhase * (beats.steps )) % 1.0);
        ;
        composerInfo.passes.staticNoise.uniforms["size"].value = 8.0 + 0.05 *
            ((player.gridPhase * (beats.steps / 2) ) % 1.0);
        ;

        //renderer.render(scene, camera);
        composerInfo.composer.render(0.1);

        if (!runRender) { return; }
        return requestAnimationFrame(animate);

        //if ( statsEnabled ) stats.update();

      }

      let activeTrack = 0;

      function setStepVolumeAt(trackId, stepId, value) {

        SAMPLE_BEATGRID.tracks[trackId].values[stepId]            = value;
        cubeGrid.values[trackId * SAMPLE_BEATGRID.steps + stepId] = value;

        cubeGrid = stepGrid.updatePhase(cubeGrid, player, true);
      }

      function setStepVolumes(stepVols, trackToUpdate = activeTrack) {

        for (let i = 0; i < 16; ++i) {
          SAMPLE_BEATGRID.tracks[trackToUpdate].values[i]            = stepVols[i];
          cubeGrid.values[trackToUpdate * SAMPLE_BEATGRID.steps + i] = stepVols[i];
        }

        cubeGrid = stepGrid.updatePhase(cubeGrid, player, true);
      }

      function setActiveTrack(trk, callback) {
        if (trk < 0 || activeTrack === trk ||
            trk >= SAMPLE_BEATGRID.tracks.length) return;

        let trackRef = SAMPLE_BEATGRID.tracks[trk];

        activeTrack = trk;
        try {
          callback(trackRef.values);
        } catch (e) {
          console.error(e);
          throw e;
        }

      }

      function getActiveTrack(trk) {
        return activeTrack;
      }

      return {
        setStepVolumes: setStepVolumes,
        setActiveTrack: setActiveTrack,
        getActiveTrack: getActiveTrack,
        trackCount    : SAMPLE_BEATGRID.tracks.length
      };
    }
;