# BeatBox Chart - Tableau JS Hackathon 2017

Interactive 3D and a fully dynamic and parametric sampler controlled from a Tableau Visualization using bi-directinal data transfer. 
Presented using three.js for interactive 3D, WebAudio for the sample playback, some pixel shaders for 80s VHS feeling and of course the Tableau JS API.

Live demo: https://brilliant-data.gitlab.io/tableau-hackathlon-17-beatbox/

[![alt text](https://pbs.twimg.com/tweet_video_thumb/DHmOW0lXkAIZktS.jpg)](https://brilliant-data.gitlab.io/tableau-hackathlon-17-beatbox/)

## Video & Voting

This project is an entry to Tableau's DataDev JS Hackathon. [You can vote us here](https://www.tableau.com/forms/javascript-api-hackathon-community-voting) and watch the video here:

[![](http://img.youtube.com/vi/_xCSS-eIwDE/0.jpg)](http://www.youtube.com/watch?v=_xCSS-eIwDE "BeatBox Chart - How it works?")

 
## It all came down to two key questions:

 * Should we do something useful? **Nah.*
 * What avenues of interactive data presentation aren't in Tableau by default? **For example: Interactive 3D and Interesting Audio. **

## The data flow:

 * our focus was on demonstrating that with the JS API, one can now think outside the box when it comes to the presentation of data
 * we kept our interface thin: we only use the set of sixteen step values and the track we are currently targeting as Tableau Parameters
 * when the user changes a value, we update the data in the beatbox based on the Parameter change event
 * when the user changes tracks, we load the step values for the current track back from the beatbox

## The visual angle:

 * we wanted to evoke the feeling of a strange, future-archaic electronic music instrument from the end of the 80's, early 90's
 * most of these instruments were modified / "Hot-rodded"  by their owner to better suit their need
 * and since we are "Hacking Tableau", we wanted it to preseve this feeling of "repurposed DIY" that was also prevalent in that era
 * we fixed our eyes on a screenshot of an old FBI warning at the start of a VHS movie 
   * we took our color palette for the "contents" screen
   * VHS as a medium has a number of signature issues that allowed us to hint at it using WebGL pixel shaders
     * noise
     * distortion
     * interlace

 * After this it was only tweaking shader and animation properties to make it look like the visual target:
   * the most crucial elements were the white animations of the currently ticking boxes, in the end they are simple cosines starting a little bit before phase 0 to give them some proper punch
   * the amount of noise, interlace and distortion are all tied to the current phase and tempo of the song, so they should underline the rhythm of the 3D visuals

## Lessons learned:

 * WebAudio connect() is expensive
 * WebGL draw calls are also expensive.... custom shaders and instancing is a must for larger-scaled scenes (especially when it comes to dynamically generated geometry like custom charts)
 * I'm hoping for a Tableau JS API with a more fine-grained Parameter update & notify architecture
