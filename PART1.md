# VHS

> Q: Should we do something useful?
>
> A: Naaahhh...


> Lets bring back some of those sweet turn-of-the-90s feelings of sunshine filtered
> through VHS scanlines.


Most of the instruments used for electronic music production in this era were
re-purposed synthesizers designed to (badly) replicate traditional instruments.
With this mindset in place, we are going to re-purpose a Tableau Dashboard as
the control surface for a sampler/beatbox.

On the visual front, the era was all about embracing the aesthetic choices
forced on creators due to the use of video tape as the primary visual media
transfer format affordable by almost anyone. Arcades lit by monochromatic green
from the vector displays of first-gen 3d games, palette-swapped tile-graphics,
the first experiments with real-time 2d scrolling.

An era where hardware limitations shaped the visual, aural and tonal culture,
and where limits were belived to be temporary restrictions until tech catches
up with us, and ideas seemed interchangable with results at times.

(This is my attempt to use this "positivity" of the era as a framing device for
this article, as we'll see with the parameter switching, a positive perspective
about the future is a must when dealing with the limitations of todays Tableau
API)


### Prelude

Please consider these posts where I try to show you (through examples), that if
you have a vision for something -no matter how strange that vision may seem-
the current batch of web technologies are more then capable of getting you
there (or at least close to it).

The actual thing that you can see as the entry is a pretty subpar thing that is
in reality a thrown together mess of javascript; so simply going through the
code would serve very little value, instead I'll try to explain how the
starting aesthetical vision became an actual thing, and hope that you'll be
able to draw your own conclusions.




## Lets make a grid

At thee heart of it, our beatbox is represented by a single table and a bunch
of metadata. For this explanation, we are looking at a single track (for
example the Kick drum track: the bottom row when looking from the default
viewpoint).

Here is a small bit from the default track:

```js

const SAMPLE_BEATGRID = {
  steps       : 16,
  bpm         : 92,
  stepsPerBeat: 4,
  tracks      : [
    // ...
  ]
};
```


`BPM` and `stepsPerBeat` define the actual step length: in this case 92 beats
per minute times 4 steps per beat should be 368(?) steps per minute, and each
step will take 60 / 368 seconds. We'll use this information for scheduling the
sample playback events.


### Anatomy of a single track - steps

Each track can contain a number of steps (from 1 to any number, but the reality
of displaying it means we'll settle for 16 steps, as that will most certanly
fit the screen.

```js
{
  name  : "Kick",
  type  : "gate",
  steps : 16,
  values: [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0.9]
},
```


Each of the steps contains a single, normalized float value: in MIDI, this
would be called `velocity`, sometimes called `strength` or `value`, and can be
used to modulate parameters of a sound on a per-hit basis. We are using the
most basic modulation: it influences the volume of the hit. The larger the
value, the stronger the hit.

Lets look at a "narrated" example for the kick drums:

```
1.0 | 0 | 0 | 0 | 1.0 | 0 | 0 | 0 | 1.0 | 0 | 0 | 0 | 1.0 | 0 | 0 | 0 |
BOOM            | BOOM            | BOOM            | BOOM            |


1.0 | 0 | 0 | 0 | 1.0 | 0 | 0 | 0 | 1.0 | 0 | 0.5 | 0 | 1.0 | 0 | 0 | 0.5|
BOOM            | BOOM            | BOOM      boom    | BOOM          ba-|
```

Now that the steps are set, lets look at the metadata we'll use for a sample"

### Anatomy of a single track - metadata

Next, we need to associate these steps with some kind of a sound.

If you've ever played around with any hardware or software samplers, this will
be pretty familiar stuff:

```js

  "Kick"      : {
    file        : "beatbox/sounds/Kick/Kick003.wav",
    start       : 0,
    length      : 0.4,
    playbackRate: 0.9,
    gain        : -24,
    lowpass     : 7000,

    valueTo: {
      gain   : 12,
      lowpass: 2000
    }
  },
```

- we need to start playback somewhere. Lets use a normalized float as a
  relative position, so dont have to care about the actual length of the
  sample.

- we need an endpoint to stop the playback. A safe bet is using a `length`
  parameter in the same fashion as `start`

- we can alter the pitch of the sample by changing its rate of playback.
  Setting a `playbackRate` of 0.5 lowers the sample an octave, 2.0 plays it an
  octave higher relative to its original pitch. In retrospect, a better unit to
  use would have been cents (1/100-th of a semitone).

- as the samples may by sampled at a different volume then our target, we need
  a gain adjustment. And while I can cope with pitch adjustments using scalars
  (as I rarely touch them in this project), I'm terrible with tuning volume
  using scalar values, so this parameter is in decibels

- most samples will contain unwanted frequencies that will mess with our audio
  spectrum and compositional tone, so we'll need a filters to cut off these
  unwanted parts. The beatbox has a `highpass` and `lowpass` filter


As I've mentionned in "Anatomy of a single track - steps", each step has a
normalized float value and that we can use this value to modulate parameters on
a per-step basis. This is where the `valueTo` field comes in: the final value
for any parameter is:

```
<value from sample metadata> + <value from step> * <valueTo[parameter name] from the sample metadata>
```

So in the example, the `gain` is linearly scaled from -24dB to (-24dB + 12dB) =
-12dB depending on the value of the step (which will result in exponential
modulation of the gain scalar... told you I was terrible with judging gain by
scalar...). `lowpass` is similarly scaled from 7000Hz to 9000Hz.


## Make some sound

We'll use the WebAudio API to get the sound going. This API is all about
building a processing graph (imagine plugging stuff into other stuff using
cables, and you're all set), connecting it to the soundcard output; then add
events with precise timestamps to schedule sample playback, or connect some
audio input to your graph.

We'll use the former, and split the job into two parts:

- the mixer, which is everything up to an individual sample voice, and is controlled via the UI
- a per-voice block which applies per-sample modulation


### Create a mixer

Our mixer models a very basic digital desk: channels are simply mixed then
passed through a compressor to control the output gain, and a highpass of 30Hz
is applied to get rid of DC offset and rumble (we are pitch-shifting
band-limited samples; low frequency takes up exponentially more space and may
change slower then the transients of other samples; so this is both a safety
precaution and a very basic dynamics fix).

With the compressor, we are going for a late 80's FET compressor vibe:

no knee, high treshold (-12dB), 2.0 ratio, moderately fast attack nothing-special release

(except with peak instead of RMS, because WebAudio, but we have highly
processed samples from 2017, and no overhead above 1.0, so we should be just
fine :) )

```js
// create the audio context
window.AudioContext = window.AudioContext || window.webkitAudioContext;

let context         = new AudioContext();

// We'll use this to chain things
let mainOut = context.destination;

// create the master output
let compressor             = context.createDynamicsCompressor();

compressor.threshold.value = -12;
compressor.knee.value      = 0;
compressor.ratio.value     = 2.0;

// 10ms attack - try let the transients through
compressor.attack.value    = 0.01;
// 170ms release - around 1/16 - 1/8T for reasonable BPM, so should work with the kick
compressor.release.value   = 0.17;


// create the master highpass before the compressor (so things
// that arent in the output signal dont trigger the compressor)
let hp             = context.createBiquadFilter();
hp.type            = "highpass";
hp.frequency.value = 30;

// connect t
hp.connect(compressor).connect(mainOut);
mainOut = hp;

```

### Create the tracks

This is where the problems with the original version start:

#### Intermission

The sample playback node in WebAudio is a single-use node, you can use a player
to play a single instance of the sound then you have to either disconnect it
manually, or leave it connected to the node graph until the user reloads the page.

As leaving all played sounds connected started giving problems after a number
of loops (depending on the memory available and other system-specific things),
so that sollution was out of the question.

As this specific feature was coded between 3am and 5am on a monday morning
after three days of non-stop coding, just coming off of the scheduling dead-end
discussed later, the brain-dead sollution I settled on was pre-allocating a
voice for each step on loop data scheduling, and disconnecting the old voices
en-masse, so instead of modifying the processing graph each step, we are
modifying it only once every 16 steps. This stopped the fan from going crazy in
my laptop, which was a good-enough quality-of-life enhancement that I left it
that way.

However, this is still bad. My current recommendation would be to mimic a
traditional poly-synth: pre-allocate a number of voices and round-robin any
playing note to them, connecting the sample player on note start and
disconnecting it on the next note start for that voice. This should be much
more efficient then the terrible-terrible large-block alloc used in the entry.

The contents of a voice is the same in either case, so lets continue it from there.

#### Back to creating the voice


Each single voice just contains each modulatable component for the voice:
`sample -> gain-adjust -> low-pass -> high-pass`

Each are defaulted to the sample metadata discussed previously:

```js

// Add defaults to the mapping data if needed
soundMapping = Object.assign({
                               file        : null,
                               gain        : -6,
                               playbackRate: 1.0,
                               lowpass     : 20000,
                               highpass    : 40
                             }, soundMapping);
```

Then the stages are created:

```js
// Setup the channel

let context        = player.context;
let hp             = context.createBiquadFilter();
hp.type            = "highpass";
hp.frequency.value = soundMapping.highpass;

let lp             = context.createBiquadFilter();
lp.type            = "lowpass";
lp.frequency.value = soundMapping.lowpass;

let gain        = context.createGain();
gain.gain.value = soundMapping.gain;

hp.connect(lp).connect(gain).connect(connectOutputTo);
let channel = hp;
```

#### Playing a sample

There is one more component missing from playing a sample: the sample data
itself. Since we are using THREE.js loaders to load the sample files, the
actual loading of samples will be discussed in the THREE.js part. For now, lets
assume that we have a nice `Float32Array` full of sample data that we want to
play (we will call him/her/it `buffer` for now)

So if we want to play a sample we connect it, set the modulated parameters:

(I've left the bookkeeping / things specific to my implementation just for
completeness, they should be considered as ways to avoid when making anything
with actual value)


```js
function _playSound(context, sound, buffer, event, voice)
{
  // housekeeping, decomposing the play event we recieved
  let {value, modulators, index} = event;
  let time                       = event.atTime;


  sound = Object.assign({}, sound);
  // apply modulation scaling
  Object.keys(modulators).forEach(param => {
    sound[param] += modulators[param][index];
  });

  let {start, length, playbackRate, gain, highpass, lowpass, valueTo} = sound;

  function scaledWithValue(base, scale, value) {
    return base + (value - 0.5) * 2 * (scale || 0);
  }

  // apply gain
  if (gain) {
    // convert back the decibels
    voice.gain.gain.value = Math.pow(10,
        scaledWithValue(gain, valueTo.gain, value) / 20);
  }

  // apply highpass
  if (highpass) {
    voice.highpass.frequency.value =
        scaledWithValue(highpass, valueTo.highpass, value);
  }

  // apply highpass
  if (lowpass) {
    voice.lowpass.frequency.value =
      scaledWithValue(lowpass, valueTo.lowpass, value);
  }

  // WebAudio browser compatibility is awesome
  if (!voice.source.start) {
    voice.source.start = voice.source.noteOn;
  }

  voice.source.playbackRate.value = playbackRate;
  voice.source.start(time, start * buffer.duration * start, buffer.duration *
      length);

}
```


## Scheduling the notes

This section should have been a nice bit about how to create a nice windowing scheduler for playing back the notes from the grid.

This did not work out, and after wasting too many hours on it, I junked it, and simply went back to re-scheduling the next loop iteration just before finishing the current one:

```
STEP     0123...         0123...
GRID     |---+---+---+---|---+---+---+---
LOOP     |loop1          |loop2
SCHEDULE        HERE ->X              X
```

So for each loop iteration, when the current timer is after the schedule start point, we schedule all events for the loop in one go, timing them using the `stepLenght` we got from the `BPM` calculation back at the start. The `_playSound()` function shown previously does just this.


### A note about clocks

For some reason, every design-by-commitee IT platform (OS-es, browsers, development envs, etc.):
- have more then one clock with different precisions
- these clocks may or may not be consistent with each other

There are three straight-forward clocks in Browser JavaScript we could use:

- Date from [`new Date()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)

- frame-time from [`requestAnimationFrame`](https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame)

- webAudio clock from [`AudioContext.currentTime`](https://developer.mozilla.org/en-US/docs/Web/API/AudioContext/currentTime)

As we are in Rome, we should do like romans do and use the webAudio clock for
anything audio-playback related. Using any of the other ones is just asking for
pain.


### Triggering the scheduler

The scheduler for the beatbox gets triggered from the `requestAnimationFrame`
callback (I did not want it to run in the background, as the thing regularly
misbehaved during development). For anything approaching production, you'll
want to trigger this from a `WebWorker` background thread, as those things are
less likely to be pre-empted by the browser on GUI interaction.

The best tutorial about this scheduling that I've found is ["A Tale of Two
Clocks - Scheduling Web Audio with Precision"](https://www.html5rocks.com/en/tutorials/audio/scheduling/),
and it does a far better job at explaining things then I ever could.


## Modulation tracks

Of course, there is more, there is always more.

If you remember in the beginning, when we were looking at the track data, it had a `type: "gate"` key-value pair, that I was suspiciously silent about. Lets talk about track types.

#### Gate tracks

You've already met the `gate` type, which is called gate because its called `gate` on sequencers. This is trigger track that triggers a sound for each step if the value of the step is greater then 0.

These tracks are displayed as the flashing boxes in the 3D view.

#### Curve tracks

There is also a `curve` track type, which is for modulating parameters on existing tracks. Lets take a look at an example `curve` track:



```js
{
  name    : "Bass pitch",
  type    : "curve",
  steps   : 16,
  forTrack: "Bass",
  targets : ["playbackRate"],
  amount  : 0.5,
  values  : [
    0,
    0.4,
    1,
    0.5,
    0.5,
    0.5,
    0.5,
    0.5,
    0.5,
    1,
    1,
    1,
    0.9,
    0.8,
    0.7,
    0.6]
},
```

As the name implies, this track modulates the `playbackRate` value (aka
pitch-shift) of the `Bass` track by `0.0 -> 0.5` depending on the value of
this `curve` track at the current step.

During playback, the current values from all modulator tracks targeting a single parameter are summed up and the sum is dispatch to the `_playSound()` function.

These tracks are displayed as line-charts in the 3D view.



## End of part 1

And with the `curve` tracks, we are finished with the audio internals of the Beatbox, in the next part, we'll take a look at the 3D pieces of the puzzle and in the final one, we'll talk about Tableau integration.

`---------------- CHANGE ADDRESS ------------------------------`

ps: for the curious ones, there is a Tableau-less version for demonstrational purposes, currently at http://stage-155.brilliant-data.net/webdataconnectors/beatbox.html

`---------------- /CHANGE ADDRESS ------------------------------`
